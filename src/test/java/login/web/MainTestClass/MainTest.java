package login.web.MainTestClass;

import org.junit.runner.JUnitCore;
import org.junit.runner.notification.Failure;

import login.web.TestClass.TestLogin;


public class MainTest {

	public static void main(String[] args) {
		
		org.junit.runner.Result result = JUnitCore.runClasses(TestLogin.class);
		
		for(Failure failure: result.getFailures()){
			System.out.println(failure.toString());
		}
		
		System.out.println(result.wasSuccessful());
	}
	
}

